#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>

#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
unsigned int vall = 0;  //obrysova svetla
unsigned int vall1 = 0;  //potkavaci svetla
unsigned int vall2 = 0;  //dalkova svetla
unsigned int vall3 = 0; //levy blinkr
unsigned int vall4 = 0;  //pravy blinkr
unsigned int can1 = 0;   //baterie

extern "C"
{
	xQueueHandle myQueue01Handle;
	xQueueHandle myQueue02Handle;
	xQueueHandle myQueue03Handle;
	xQueueHandle myQueue04Handle;
	xQueueHandle myQueue05Handle;
	xQueueHandle myQueue06Handle;
}


Model::Model() : modelListener(0)
{
	myQueue01Handle = xQueueGenericCreate(1, 1, 0);
	myQueue02Handle = xQueueGenericCreate(1, 1, 0);
	myQueue03Handle = xQueueGenericCreate(1, 1, 0);
	myQueue04Handle = xQueueGenericCreate(1, 1, 0);
	myQueue05Handle = xQueueGenericCreate(1, 1, 0);
	myQueue06Handle = xQueueGenericCreate(1, 1, 0);
}

void Model::tick()
{
	if(xQueueReceive(myQueue01Handle, &vall, 0) == pdTRUE){
		//something but it will slow down GUI
		modelListener->setNewValue(vall);     //obrysova svetla
	}
	if(xQueueReceive(myQueue02Handle, &vall1, 0) == pdTRUE){
			//something but it will slow down GUI
			modelListener->setNewValue(vall1);   //potkavaci svetla
		}
	if(xQueueReceive(myQueue03Handle, &vall2, 0) == pdTRUE){
			//something but it will slow down GUI
			modelListener->setNewValue(vall2);  //dalkova svetla
		}
	if(xQueueReceive(myQueue04Handle, &vall3, 0) == pdTRUE){
			//something but it will slow down GUI
			modelListener->setNewValue(vall3);  //levy blinkr
		}
	if(xQueueReceive(myQueue05Handle, &vall4, 0) == pdTRUE){
			//something but it will slow down GUI
			modelListener->setNewValue(vall4);  //pravy blinkr
		}

	if(xQueueReceive(myQueue06Handle, &can1, 0) == pdTRUE){
			//something but it will slow down GUI
			modelListener->setCAN(can1);   //kapacita baterie v %
		}
}
