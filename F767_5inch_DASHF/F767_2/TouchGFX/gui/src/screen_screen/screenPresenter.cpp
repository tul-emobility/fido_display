#include <gui/screen_screen/screenView.hpp>
#include <gui/screen_screen/screenPresenter.hpp>

screenPresenter::screenPresenter(screenView& v)
    : view(v)
{

}

void screenPresenter::activate()
{

}

void screenPresenter::deactivate()
{

}

void screenPresenter::setNewValue(unsigned int value)
{
	view.updateVal(value);
}
void screenPresenter::setCAN(unsigned int value)
{
	view.updateCAN(value);
}
