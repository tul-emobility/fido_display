#include <gui/screen_screen/screenView.hpp>
#include <touchgfx/Color.hpp>
#include "BitmapDatabase.hpp"
#include <texts/TextKeysAndLanguages.hpp>

screenView::screenView() {

}

void screenView::setupScreen() {
	//POWER
	powerProgress.setXY(405, 383);
	powerProgress.setProgressIndicatorPosition(0, 0, 392, 55);
	powerProgress.setRange(0, 100);
	powerProgress.setDirection(touchgfx::AbstractDirectionProgress::RIGHT);
	powerProgress.setBackground(
			touchgfx::Bitmap(BITMAP_RIGHTGRAYBACKGROUND_ID));
	powerProgress.setBitmap(BITMAP_RIGHTGREENBACKGROUND_ID);
	powerProgress.setValue(60);
	powerProgress.setAnchorAtZero(false);
	add(powerProgress);

	//CHARGE
	chargeProgress.setXY(83, 383);
	chargeProgress.setProgressIndicatorPosition(0, 0, 310, 54);
	chargeProgress.setRange(0, 100);
	chargeProgress.setDirection(touchgfx::AbstractDirectionProgress::LEFT);
	chargeProgress.setBackground(
			touchgfx::Bitmap(BITMAP_LEFTGRAYBACKGROUND_ID));
	chargeProgress.setBitmap(BITMAP_LEFTGREENBACKGROUND_ID);
	chargeProgress.setValue(60);
	chargeProgress.setAnchorAtZero(false);
	add(chargeProgress);

	batteryProgress.setXY(0, 155);
	batteryProgress.setProgressIndicatorPosition(0, 0, 58, 285);
	batteryProgress.setRange(0, 100);
	batteryProgress.setDirection(touchgfx::AbstractDirectionProgress::UP);
	batteryProgress.setBackground(touchgfx::Bitmap(BITMAP_BATTERYGRAY_ID));
	batteryProgress.setBitmap(BITMAP_BATTERY_ID);
	batteryProgress.setValue(60);
	batteryProgress.setAnchorAtZero(false);
	add(batteryProgress);

	powerText.setXY(434, 394);
	powerText.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
	powerText.setLinespacing(0);
	powerText.setTypedText(touchgfx::TypedText(T_POWER));
	add(powerText);

	chargeText.setXY(238, 394);
	chargeText.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
	chargeText.setLinespacing(0);
	chargeText.setTypedText(touchgfx::TypedText(T_CHARGE));
	add(chargeText);

	line6.setPosition(0, 326, 59, 19);
	line6Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
	line6.setPainter(line6Painter);
	line6.setStart(0, 0);
	line6.setEnd(57, 0);
	line6.setLineWidth(1);
	line6.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

	line8.setPosition(0, 382, 59, 19);
	line8Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
	line8.setPainter(line8Painter);
	line8.setStart(0, 0);
	line8.setEnd(57, 0);
	line8.setLineWidth(1);
	line8.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

	line4.setPosition(0, 268, 59, 19);
	line4Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
	line4.setPainter(line4Painter);
	line4.setStart(0, 0);
	line4.setEnd(57, 0);
	line4.setLineWidth(1);
	line4.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

	line2.setPosition(0, 212, 59, 19);
	line2Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
	line2.setPainter(line2Painter);
	line2.setStart(0, 0);
	line2.setEnd(57, 0);
	line2.setLineWidth(1);
	line2.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

	line3.setPosition(0, 240, 59, 19);
	line3Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
	line3.setPainter(line3Painter);
	line3.setStart(0, 0);
	line3.setEnd(57, 0);
	line3.setLineWidth(1);
	line3.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

	line5.setPosition(0, 296, 59, 19);
	line5Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
	line5.setPainter(line5Painter);
	line5.setStart(0, 0);
	line5.setEnd(57, 0);
	line5.setLineWidth(1);
	line5.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

	line7.setPosition(0, 354, 59, 19);
	line7Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
	line7.setPainter(line7Painter);
	line7.setStart(0, 0);
	line7.setEnd(57, 0);
	line7.setLineWidth(1);
	line7.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

	line9.setPosition(0, 410, 59, 19);
	line9Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
	line9.setPainter(line9Painter);
	line9.setStart(0, 0);
	line9.setEnd(57, 0);
	line9.setLineWidth(1);
	line9.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

	line1.setPosition(0, 184, 59, 19);
	line1Painter.setColor(touchgfx::Color::getColorFrom24BitRGB(0, 0, 0));
	line1.setPainter(line1Painter);
	line1.setStart(0, 0);
	line1.setEnd(57, 0);
	line1.setLineWidth(1);
	line1.setLineEndingStyle(touchgfx::Line::ROUND_CAP_ENDING);

	lights.setVisible(false);
	lights.invalidate();

	batteryPercent.setXY(4, 211);
	batteryPercent.setColor(
			touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
	batteryPercent.setLinespacing(0);
	Unicode::snprintf(percentBuffer, PERCENT_SIZE, "%s",
			touchgfx::TypedText(T_BATTERYP).getText());
	batteryPercent.setWildcard(percentBuffer);
	batteryPercent.resizeToCurrentText();
	batteryPercent.setTypedText(touchgfx::TypedText(T_BATTERYP));

	range.setXY(5, 271);
	range.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
	range.setLinespacing(0);
	range.setTypedText(touchgfx::TypedText(T_RANGEKM));

	rangeUnit.setXY(10, 294);
	rangeUnit.setColor(touchgfx::Color::getColorFrom24BitRGB(255, 255, 255));
	rangeUnit.setLinespacing(0);
	rangeUnit.setTypedText(touchgfx::TypedText(T_RANGEUNITTEXT));

	add(line1);
	add(line2);
	add(line3);
	add(line4);
	add(line5);
	add(line6);
	add(line7);
	add(line8);
	add(line9);
	add(rangeUnit);
	add(batteryPercent);
	add(range);
}

void screenView::tearDownScreen() {
	screenViewBase::tearDownScreen();
}
void screenView::handleTickEvent() {

	topIcons(false);
//	tickCounter++;
//	if (tickCounter % 60 == 0) {
//		changeGear(mode);
//		changeLights(lightsMode);
//		topIcons(topMode);
//		runWinkers(winkers);
//		lightsMode++;
//		mode++;
//
//		if (winkers) {
//			winkers = false;
//		} else {
//			winkers = true;
//		}
//
//		if (topMode) {
//			topMode = false;
//		} else {
//			topMode = true;
//		}
//
//		if (mode > 2) {
//			mode = 0;
//		}
//
//		if (lightsMode > 3) {
//			lightsMode = 0;
//		}
//
//	}
//
//	if (tickCounter % 5 == 0) {
//
//		if (value > 100) {
//			direction = false;
//		} else if (value == 0) {
//			direction = true;
//		}
//
//		if (direction) {
//			value++;
//		} else {
//			value--;
//		}
//
//		powerProgress.setValue(value);
//		chargeProgress.setValue(value);
//		batteryProgress.setValue(value);
//		if (value < 15) {
//			chargingGreen.setVisible(false);
//			chargingGreen.invalidate();
//			chargingRed.setVisible(true);
//			chargingRed.invalidate();
//		} else {
//			chargingGreen.setVisible(true);
//			chargingGreen.invalidate();
//			chargingRed.setVisible(false);
//			chargingRed.invalidate();
//
//		}
//	}
}

void screenView::changeLights(int lightsMode) {

	switch (lightsMode) {
		case 0:
		lightsLow.setVisible(true);
		lightsLow.invalidate();
		lights.setVisible(true);
		lights.invalidate();
		lightsLong.setVisible(true);
		lightsLong.invalidate();
		break;

		case 1:
		lightsLow.setVisible(false);
		lightsLow.invalidate();
		break;

		case 2:
		lights.setVisible(false);
		lights.invalidate();
		break;

		case 3:
		lightsLong.setVisible(false);
		lightsLong.invalidate();
		break;

	}
}

void screenView::changeGear(int mode) {
	switch (mode) {
		case 0:
		gearD.setColor(touchgfx::Color::getColorFrom24BitRGB(102, 102, 102));
		gearD.invalidate();
		gearN.setColor(touchgfx::Color::getColorFrom24BitRGB(49, 219, 40));
		gearN.invalidate();
		break;

		case 1:
		gearN.setColor(touchgfx::Color::getColorFrom24BitRGB(102, 102, 102));
		gearN.invalidate();
		gearR.setColor(touchgfx::Color::getColorFrom24BitRGB(49, 219, 40));
		gearR.invalidate();
		break;

		case 2:
		gearR.setColor(touchgfx::Color::getColorFrom24BitRGB(102, 102, 102));
		gearR.invalidate();
		gearD.setColor(touchgfx::Color::getColorFrom24BitRGB(49, 219, 40));
		gearD.invalidate();
		break;

	}
}

void screenView::topIcons(bool topMode) {
	if (mode) {
		handbrake.setVisible(true);
		handbrake.invalidate();
		brakeError.setVisible(true);
		brakeError.invalidate();
		engineError.setVisible(true);
		engineError.invalidate();
		warningLights.setVisible(true);
		warningLights.invalidate();
		overheat.setVisible(true);
		overheat.invalidate();
		mainBatteryError.setVisible(true);
		mainBatteryError.invalidate();
		batteryError.setVisible(true);
		batteryError.invalidate();
	} else {
		handbrake.setVisible(false);
		handbrake.invalidate();
		brakeError.setVisible(false);
		brakeError.invalidate();
		engineError.setVisible(false);
		engineError.invalidate();
		warningLights.setVisible(false);
		warningLights.invalidate();
		overheat.setVisible(false);
		overheat.invalidate();
		mainBatteryError.setVisible(false);
		mainBatteryError.invalidate();
		batteryError.setVisible(false);
		batteryError.invalidate();
	}
}

void screenView::runWinkers(bool winkers) {
	if (winkers) {
		leftWinker.setVisible(true);
		rightWinker.setVisible(true);
		leftWinker.invalidate();
		rightWinker.invalidate();
	} else {
		leftWinker.setVisible(false);
		rightWinker.setVisible(false);
		leftWinker.invalidate();
		rightWinker.invalidate();
	}

}

void screenView::updateVal(unsigned int variable) {
//	Unicode::snprintf(speedBuffer, SPEED_SIZE,"%d", newValue);
//	speed.resizeToCurrentText();
//	speed.invalidate();

	if (variable/10 == 1) {
		if (variable - 10 == 0) {
			lightsLow.setVisible(false);
			lightsLow.invalidate();
		} else {
			lightsLow.setVisible(true);
			lightsLow.invalidate();
		}
	}

	if (variable/10 == 2) {
		if (variable - 20 == 0) {
			lights.setVisible(false);
			lights.invalidate();
		} else {
			lights.setVisible(true);
			lights.invalidate();
		}
	}


	if (variable/10 == 3) {
		if (variable-30 == 0) {
			lightsLong.setVisible(false);
			lightsLong.invalidate();
		} else {
			lightsLong.setVisible(true);
			lightsLong.invalidate();
		}
	}


	if (variable/10 == 4) {
		if (variable-40 == 0) {
			leftWinker.setVisible(false);
			leftWinker.invalidate();
		} else {
			leftWinker.setVisible(true);
			leftWinker.invalidate();
		}
	}


	if (variable/10 == 5) {
		if (variable-50 == 0) {
			rightWinker.setVisible(false);
			rightWinker.invalidate();
		} else {
			rightWinker.setVisible(true);
			rightWinker.invalidate();
		}
	}
//
//
//	if (variable/10 == 6) {
//		if (variable-60 == 0) {
//			handbrake.setVisible(false);
//			handbrake.invalidate();
//		} else {
//			handbrake.setVisible(true);
//			handbrake.invalidate();
//		}
//	}

}

void screenView::updateCAN(unsigned int variable) {

	Unicode::snprintf(percentBuffer, PERCENT_SIZE,"%d", variable);
	batteryPercent.resizeToCurrentText();
	batteryPercent.invalidate();

	batteryProgress.setValue(variable);
	batteryProgress.invalidate();
	if(variable < 15) {
		chargingGreen.setVisible(false);
		chargingGreen.invalidate();
		chargingRed.setVisible(true);
		chargingRed.invalidate();
	} else {
		chargingGreen.setVisible(true);
		chargingGreen.invalidate();
		chargingRed.setVisible(false);
		chargingRed.invalidate();
	}
//		Unicode::snprintf(speedBuffer, SPEED_SIZE,"%d", variable);
//		speed.resizeToCurrentText();
//		speed.invalidate();
}
