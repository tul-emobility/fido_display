#ifndef MODELLISTENER_HPP
#define MODELLISTENER_HPP

#include <gui/model/Model.hpp>

class ModelListener
{
public:
    ModelListener() : model(0) {}
    
    virtual ~ModelListener() {}

    void bind(Model* m)
    {
        model = m;
    }
    virtual void setNewValue(unsigned int value){}
    virtual void setCAN(unsigned int value){}
protected:
    Model* model;
};

#endif // MODELLISTENER_HPP
