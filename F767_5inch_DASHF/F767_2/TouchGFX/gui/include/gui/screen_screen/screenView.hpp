#ifndef SCREENVIEW_HPP
#define SCREENVIEW_HPP

#include <gui_generated/screen_screen/screenViewBase.hpp>
#include <gui/screen_screen/screenPresenter.hpp>
#include <touchgfx/widgets/TextArea.hpp>
#include <touchgfx/widgets/Image.hpp>
#include <touchgfx/widgets/canvas/Line.hpp>
#include <touchgfx/widgets/canvas/PainterRGB565.hpp>
#include <touchgfx/containers/progress_indicators/ImageProgress.hpp>
#include <touchgfx/widgets/TextAreaWithWildcard.hpp>

class screenView : public screenViewBase
{
public:
    screenView();
    virtual ~screenView() {}
    virtual void setupScreen();
    virtual void tearDownScreen();
    virtual void handleTickEvent();
    virtual void changeGear(int mode);
    virtual void changeLights(int lightsMode);
    virtual void topIcons(bool topMode);
    virtual void runWinkers(bool winkers);

    void updateVal(unsigned int newValue);
    void updateCAN(unsigned int newValue);
protected:

	int value;
	int tickCounter;
	int mode;
	int lightsMode;
	bool direction;
	bool topMode;
	bool winkers;
	touchgfx::ImageProgress powerProgress;
	touchgfx::ImageProgress chargeProgress;
	touchgfx::ImageProgress batteryProgress;
	touchgfx::TextArea powerText;
	touchgfx::TextArea chargeText;
    //touchgfx::TextArea batteryPercent;
    touchgfx::TextArea range;
    touchgfx::TextArea rangeUnit;
    touchgfx::TextAreaWithOneWildcard batteryPercent;
	touchgfx::Line line1;
    touchgfx::PainterRGB565 line1Painter;
    touchgfx::Line line2;
    touchgfx::PainterRGB565 line2Painter;
    touchgfx::Line line3;
    touchgfx::PainterRGB565 line3Painter;
    touchgfx::Line line4;
    touchgfx::PainterRGB565 line4Painter;
    touchgfx::Line line5;
    touchgfx::PainterRGB565 line5Painter;
    touchgfx::Line line6;
    touchgfx::PainterRGB565 line6Painter;
    touchgfx::Line line7;
    touchgfx::PainterRGB565 line7Painter;
    touchgfx::Line line8;
    touchgfx::PainterRGB565 line8Painter;
    touchgfx::Line line9;
    touchgfx::PainterRGB565 line9Painter;


    static const uint16_t PERCENT_SIZE = 3;
    touchgfx::Unicode::UnicodeChar percentBuffer[PERCENT_SIZE];
};

#endif // SCREENVIEW_HPP
